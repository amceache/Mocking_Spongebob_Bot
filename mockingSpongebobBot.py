from info import access_token, groupID, mspongeID
import requests
import json
import time
import random
import signal
import sys

class bee_bot:
    def __init__(self):
        self.gropume_url = 'https://api.groupme.com/v3/bots?token='
        self.post_url = 'https://api.groupme.com/v3/bots/post'
        self.group_url = 'https://api.groupme.com/v3/groups/'
        self.picture_url = 'https://i.groupme.com/650x381.jpeg.f9085bf3ceb9475e8a32bc4ef991fcd0'
        self.groupID = groupID
        self.groupname = 'Joey Jonas: Shirtless Once and For All'
        self.botname = 'mocking spongebob'
        self.botID = mspongeID
        self.botUserID = '553650'
        self.latestTimeStamp = time.time()

    def hello(self):
        text = {}
        text['bot_id'] = self.botID
        text['text'] = 'Hi everyone!'
        r = requests.post(self.post_url, data=json.dumps(text))

    def goodbye(self):
        text = {}
        text['bot_id'] = self.botID
        text['text'] = 'Alanna killed me :( hope to see you again soon!'
        print("bye")
        r = requests.post(self.post_url, data=json.dumps(text))
                    
    def copy_text(self, text):
        textbody = {}
        textbody['bot_id'] = self.botID
        textbody['text'] = text
        textbody['attachments'] = [{'type' : 'image', 'url' : self.picture_url}]

        r = requests.post(self.post_url, data=json.dumps(textbody))

    def loop(self):
        r_params = {'token' : access_token}
        while True:
            try:
                r = requests.get(self.group_url + self.groupID + '/messages', params = r_params)
                resp = json.loads(r.content)['response']['messages']
                for message in resp:
                        if (float(message['created_at']) > self.latestTimeStamp) and message['name'] != self.botname and message['sender_id'] == '551831':
                            self.latestTimeStamp = float(message['created_at'])
                            self.copy_text(self.randomUpperLower(message['text']))
                time.sleep(5)
            except KeyboardInterrupt:
                self.goodbye()
                sys.exit()

    def randomUpperLower(self, text):
        newtext=''
        for i in text:
            UorL = bool(random.getrandbits(1))
            if UorL:
                newtext += i.upper()
            else:
                newtext += i.lower()
        return newtext

if __name__ == '__main__':
        bbot = bee_bot()
        bbot.hello()
        bbot.loop()
