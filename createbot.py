from info import access_token, groupID
import requests
import json

class bee_bot:
    def __init__(self):
            self.groupme_url = 'https://api.groupme.com/v3/bots?token='
            self.groupID = groupID
            self.groupname = 'Joey Jonas: Shirtless Once and for All'
            self.botname = 'mocking spongebob'
            self.botID = ''

    def create_bot(self):
            createjson = {}
            createjson['bot'] = {'name' : self.botname, 'group_id' : self.groupID}
            r = requests.post(self.groupme_url + access_token, data=json.dumps(createjson))
            resp = json.loads(r.content)
            self.botID = resp['response']['bot']['bot_id']
            print self.botID

if __name__ == '__main__':
    bbot = bee_bot()
    bbot.create_bot()
